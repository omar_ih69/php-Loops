<!doctype html>
<html><body>
    
    <pre>
    The foreach loop works only on arrays,
    and is used to loop through each key/value pair in an array.
    For every loop iteration, the value of the current array element
    is assigned to $value and the array pointer is moved by one,
    until it reaches the last array element.

    </pre>
    
    
    
    <?php
    $colors = array("red", "green", "blue", "yellow");
    
    foreach ($colors as $value) {
        echo "$value <br>";
    }
    ?>
    
    
    
    
    
    
    
    
    </body></html>