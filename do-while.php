<!doctype html>
<html>
<body>
    
    <p>The example below first sets a variable $x to 1 ($x = 1). Then, the do while loop will write some output, and then increment the variable $x with 1. Then the condition is checked (is $x less than, or equal to 5?), and the loop will continue to run as long as $x is less than, or equal to 5</p>
    <?php
    $x = 1;
    
    do {
        echo "The number is: $x <br>";
        $x++;
    } while ($x <= 5);
    ?>
    
    <p>Notice that in a do while loop the condition is tested AFTER executing the statements within the loop. This means that the do while loop would execute its statements at least once, even if the condition is false the first time.

The example below sets the $x variable to 6, then it runs the loop, and then the condition is checked:</p>

    <?php 
    $x = 6;
    
    do {
        echo "The Number is: $x <br>";
        $x++;
        
    } while ($x <= 5);
    ?>
    </body>
</html>