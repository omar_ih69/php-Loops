<!doctype html>
<html>
<body>
    <pre>
    for (init counter; test counter; increment counter) {
    code to be executed;
}
Parameters:

init counter: Initialize the loop counter value
test counter: Evaluated for each loop iteration. If it evaluates to TRUE, the loop continues. If it evaluates to FALSE, the loop ends.
increment counter: Increases the loop counter value
The example below displays the numbers from 0 to 10:
</pre>
    <?php
    for ($x = 1; $x <= 10; $x++) {
        echo "The number is: $x <br>";
    }
    ?>
    
    </body>
</html>